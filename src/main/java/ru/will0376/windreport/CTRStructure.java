package ru.will0376.windreport;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.common.MinecraftForge;
import ru.will0376.windevents.events.EventRequestScreen;
import ru.will0376.windevents.utils.Utils;

import java.util.ArrayList;

public class CTRStructure {
	public String playerNick;
	public String customerNick;
	public long time;

	public CTRStructure(String player, String customer) throws Exception {
		if (player == null) throw new NullPointerException("Player not found");
		if (checkWhiteList(customer, playerNick)) throw new Exception("You have no rights to this action!");
		this.time = System.currentTimeMillis();
		this.playerNick = player;
		this.customerNick = customer;
	}

	private static EntityPlayerMP getEPMP(String nick) {
		for (EntityPlayerMP player : Utils.getAllPlayers())
			if (player.getDisplayName().equals(nick)) return player;
		return null;
	}

	/**
	 * return true if cooldown enable.
	 */
	public boolean cooldown() {
		if (customerNick.equals("Server")) return false;
		if (getEPMP(customerNick).canCommandSenderUseCommand(4, "ctmodules.report.admin.cooldownbypass")) return false;
		return time + (Reports.config.getTimeout() * 60000) > System.currentTimeMillis();
	}

	public void makeScreen() {
		MinecraftForge.EVENT_BUS.post(new EventRequestScreen(customerNick, playerNick, false, true));
	}

	/**
	 * @return true, if player in WL
	 */
	private boolean checkWhiteList(String customer, String player) {
		if (customer.equals("Server")) return false;
		if (getEPMP(customer).canCommandSenderUseCommand(4, "ctmodules.report.admin.reportwlbypass")) return false;
		ArrayList<String> whitelist = (ArrayList<String>) Reports.config.getWhitelistPC();
		return whitelist.contains(player);
	}

}

