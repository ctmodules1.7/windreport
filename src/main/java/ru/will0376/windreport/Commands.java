package ru.will0376.windreport;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import ru.will0376.windevents.utils.ChatForm;
import ru.will0376.windevents.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Commands extends CommandBase {
	public static String usage = " /" + Reports.config.getMainCommand() + " <player>";
	public static String aliases = " Aliases: [" + String.join(", ", Reports.config.getAliases()) + "]";
	public static String permissions = " Permissions: [\n" +
			"   ctmodules.report.admin.cooldownbypass\n" +
			"   ctmodules.report.admin.reportwlbypass\n" +
			"   ctmodules.report.admin.blacklist\n" +
			" ]";

	@Override
	public String getCommandName() {
		return Reports.config.getMainCommand();
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return usage;
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
		execute(sender,args);
	}
	public void execute(ICommandSender sender, String[] args) throws CommandException {
		if (Reports.config.isEnabled())
			try {
				if (args.length == 0) {
					Utils.printWithCase(sender,ChatForm.prefix,Utils.fromCaseToList(Utils.makeUsage(Reports.MOD_ID, Commands.usage, Commands.aliases, ""),"\n"));
					return;
				}
				if (sender.canCommandSenderUseCommand(4, "ctmodules.report.admin.blacklist")
						&& !sender.canCommandSenderUseCommand(4, "ctmodules.report.admin.reportwlbypass")) {
					sender.addChatMessage(new ChatComponentText(ChatForm.report_prefix_error + EnumChatFormatting.RED + "You're on the blacklist!"));
					return;
				}
				if (Reports.PCCooldown.containsKey(sender.getCommandSenderName())
						&& Reports.PCCooldown.get(sender.getCommandSenderName()).cooldown()) {
					sender.addChatMessage(new ChatComponentText(ChatForm.report_prefix_error + String.format("Cooldown: %s sec",
							(Reports.PCCooldown.get(sender.getCommandSenderName()).time + (Reports.config.getTimeout() * 60000) - System.currentTimeMillis()) / 1000)));
					return;
				}

				CTRStructure pc = new CTRStructure(args[0], sender.getCommandSenderName());
				Reports.PCCooldown.put(sender.getCommandSenderName(), pc);
				pc.makeScreen();
				sender.addChatMessage(new ChatComponentText(ChatForm.report_prefix + "Thanks!"));
			} catch (Exception e) {
				sender.addChatMessage(new ChatComponentText(ChatForm.report_prefix_error + EnumChatFormatting.RED + e.getMessage()));
			}
	}

	@Override
	public List getCommandAliases() {
		ArrayList<String> list = new ArrayList<>(Reports.config.getAliases());
		if (list.isEmpty()) return new ArrayList<>();
		return list;
	}

	@Override
	public List addTabCompletionOptions(ICommandSender sender, String[] args) {
		return args.length == 1 ? getListOfStringsMatchingLastWord(args, Utils.getAllPlayers().stream().map(EntityPlayer::getDisplayName).toArray(String[]::new)) : Collections.emptyList();
	}
}

