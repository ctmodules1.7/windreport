package ru.will0376.windreport;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import ru.will0376.windevents.events.EventPrintHelp;
import ru.will0376.windevents.events.EventReloadConfig;
import ru.will0376.windevents.utils.ChatForm;
import ru.will0376.windevents.utils.Logger;
import ru.will0376.windevents.utils.Utils;

public class Events {
	@SubscribeEvent
	public static void printHelp(EventPrintHelp e) {
		//e.printToPlayer(Utils.makeUsage(Ctreport.MOD_ID, Commands.usage, Commands.aliases, Commands.permissions));
		Utils.printWithCase(e.getPlayer(), ChatForm.prefix, Utils.fromCaseToList(Utils.makeUsage(Reports.MOD_NAME, Commands.usage, Commands.aliases, Commands.permissions), "\n"));
	}

	@SubscribeEvent
	public static void reload(EventReloadConfig e) {
		Reports.config.launch();
		if (Utils.getEPMP(e.getSender()) == null) Logger.log(1, "[Ctreport]", "Reloaded");
		else
			Utils.getEPMP(e.getSender()).addChatMessage(new ChatComponentText(EnumChatFormatting.GOLD + "[Ctreport] Reloaded"));
	}

}
