package ru.will0376.windreport;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import net.minecraft.launchwrapper.Launch;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.common.MinecraftForge;
import ru.will0376.windevents.events.EventLogPrint;
import ru.will0376.windevents.events.EventModStatusResponse;

import java.util.HashMap;

@Mod(
		modid = Reports.MOD_ID,
		name = Reports.MOD_NAME,
		version = Reports.VERSION,
		dependencies = "required-after:windevents@[1.0,)",
		acceptableRemoteVersions = "*"
)
public class Reports {

	public static final String MOD_ID = "windreport";
	public static final String MOD_NAME = "WindReport";
	public static final String VERSION = "1.0";
	public static boolean DEBUG = true;
	public static Config config;
	public static HashMap<String, CTRStructure> PCCooldown = new HashMap<>(); //user,pojo.


	@Mod.Instance(MOD_ID)
	public static Reports INSTANCE;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		if (DEBUG) DEBUG = (boolean) Launch.blackboard.get("fml.deobfuscatedEnvironment");
		config = new Config(event.getSuggestedConfigurationFile());
		config.launch();
		FMLCommonHandler.instance().bus().register(new Events());
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {

	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {
		MinecraftForge.EVENT_BUS.post(new EventLogPrint(3, EnumChatFormatting.GOLD + "[Ctreport]" + EnumChatFormatting.RESET, "PostInit done!"));
		MinecraftForge.EVENT_BUS.post(new EventModStatusResponse(MOD_ID, config.isEnabled()));
	}

	@Mod.EventHandler
	public void serverStarting(FMLServerStartingEvent e) {
		e.registerServerCommand(new Commands());
	}
}

